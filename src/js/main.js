import React from 'react';
import ReactDOM from 'react-dom';

import {Router, Route, hashHistory} from 'react-router';

import {Provider} from 'react-redux';

import GreyLoad from './GreyLoud';

import store from './configureStore';

import '../assets/scss/main.scss';

const Root = ({store}) => (
  <Provider store={store}>
    <Router>
      <Route path="/" component={GreyLoad}/>
    </Router>
    <GreyLoad/>
  </Provider>
);

ReactDOM.render(
  Root,
  document.getElementById('main')
);