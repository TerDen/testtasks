import React from 'react';

export default function Button (props) {
  return (
    <button className={props.className + ' btn btn-success' } type='submit' id={props.id}>
      {props.name}
    </button>
  )
}