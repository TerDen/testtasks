import React from 'react';

import SideNav from './SideNav';
import Content from './Content';
import {connect} from "react-redux";

class AfterEnterBlock extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      login: 'Petya',
      contentType: '1'
    };

    this.navElClickHandler = this.navElClickHandler.bind(this);
    this.getNeedUser = this.getNeedUser.bind(this);
  }

  navElClickHandler(elId) {
    this.setState({
      contentType: elId
    });

    this.props.handlerShowAllMessages(this.state.login);
  }

  getNeedUser() {
    let login = this.state.login,
      users = this.props.userInfo,
      usersLength = users.length,
      i,
      needUserMessages;

    for (i = 0; i < usersLength; i += 1) {
      if (users[i].user__login === login) {
        needUserMessages = users[i];
        return needUserMessages;
      }
    }
  }

  render() {

    return(
      <div className='AfterEnterBlock'>
        <SideNav
          onListElClickHandler = {this.navElClickHandler}
          className='AfterEnterBlock__sideNav'/>
        <Content data={this.state.contentType === '2' || '3' ? this.getNeedUser() : ''} type={this.state.contentType} className='AfterEnterBlock__content'/>
      </div>
    )
  }
}

export default connect(
  state => ({
    userInfo: state.afterEnter.users,
  }),
  dispatch => ({
    handlerShowAllMessages: (login) => {
      let payload = {
        'login': login
      };

      dispatch({type: 'USER_ALLMESSAGE', payload});
    }
  })
)(AfterEnterBlock);