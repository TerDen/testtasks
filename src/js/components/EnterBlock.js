import React from 'react';

import Button from './Button';
import Field from './Field';

export default class EnterBlock extends React.Component {
  render() {
    return (
      <div className='EnterBlock'>
        <ul className="nav nav-tabs EnterBlock__nav" role="tablist">
          <li role="presentation" className="EnterBlock__li active"><a href="#home" aria-controls="home" role="tab"
                                                                       data-toggle="tab">Registration</a></li>
          <li role="presentation" className="EnterBlock__li"><a href="#profile" aria-controls="profile" role="tab"
                                                                data-toggle="tab">Enter</a></li>
        </ul>

        <div className="tab-content EnterBlock__tab-content">
          <div role="tabpanel" className="tab-pane active" id="home">
            <form className='form_register'>
              <Field className='EnterBlock__field EnterBlock__field_row'
                     id='user__login'
                     text='Enter your login'/>
              <div className='fieldsRow'>
                <Field
                  className='EnterBlock__field'
                  id='user__firstName'
                  text='Enter your first name'/>

                <Field
                  className='EnterBlock__field'
                  id='user__secondName'
                  text='Enter your second name'/>
              </div>

              <div className='fieldsRow'>
                <Field
                  className='EnterBlock__field'
                  id='user__password'
                  text='Enter password'
                  type='password'/>
                <Field
                  className='EnterBlock__field'
                  id='user__password2'
                  text='Confirm password'
                  type='password'/>
              </div>

              <Button
                className='EnterBlock__button'
                id='registerBtn'
                name='Register'/>
            </form>
          </div>
          <div role="tabpanel" className="tab-pane" id="profile">
            <form className='form_enter'>
              <Field
                className='EnterBlock__field EnterBlock__field_row'
                id='account__login'
                text='Enter your first name'/>

              <Field
                className='EnterBlock__field EnterBlock__field_row _margin-top-10'
                id='account__password'
                text='Enter password'
                type='password'/>

              <Button
                className='EnterBlock__button'
                id='enterBtn'
                name='Enter'/>
            </form>
          </div>
        </div>
      </div>
    )
  }
}