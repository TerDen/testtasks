import React from 'react';

export default function Exit(props) {
  return(
    <div className='Exit' title='Exit' onClick={props.exitClickHandler}>
      <span className='Exit__login'>{props.login}</span>
      <i className="Exit__icon fas fa-sign-out-alt" />
    </div>
  )
}