import React from 'react';

export default class SideNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeId: 1
    };

    this.handlerNavElClick = this.handlerNavElClick.bind(this);
  }

  handlerNavElClick(e) {
    this.setState({
      activeId: +(e.target.id)
    });
    this.props.onListElClickHandler(e.target.id);
  }


  render() {

    const pages = [
      {
        id: 1,
        name: 'New message',
      },
      {
        id: 2,
        name: 'Messages',
      },
      {
        id: 3,
        name: 'My info',
      }

    ];

    let pagesList = pages.map((page, i) =>
      <li
        className={
          this.state.activeId === page.id ? 'SideNav__li_active SideNav__li' : 'SideNav__li'
        }
        key={i}
        onClick={this.handlerNavElClick}>
        <a href='#' id={page.id}>
          {page.name}
        </a>
      </li>
    );

    return (
      <nav className={this.props.className + ' SideNav'}>
        <ul>
          {pagesList}
        </ul>
      </nav>
    )
  }
}