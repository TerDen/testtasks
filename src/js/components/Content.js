import React from 'react';
import {connect} from 'react-redux';

import Field from './Field';
import Button from './Button';

export default class Content extends React.Component {
  render() {
    let type = this.props.type, childBlock, userInfo,
      data = this.props.data ? this.props.data : '';

    switch (type) {
      case '1':
        childBlock =
          <form className='Content__form'>
            <Field
              className='Content__field'
              id='content__theme'
              text='Enter message theme'/>

            <Field
              className='Content__field'
              id='content__theme'
              text='Enter message theme'
              type='textarea'/>

            <Button
              className='Content__btn _margin-top-10'
              id='sendMessage'
              name='Send message'/>
          </form>;
        break;
      case '2':
        data = data.user__messages;
        userInfo = data.map((user, i) =>
          <div className='ContentMessages__item _margin-top-20' key={i}>
            <span className='Content__label'>Message theme:</span>
            <h3 className='ContentMessages__theme _margin-top-10'>{user.message__theme}</h3>
            <span className='Content__label _margin-top-20'>Message text:</span>
            <p className='ContentMessages__text _margin-top-10'>{user.message__text}</p>

            <span className='ContentMessages__time'>{user.message__data}</span>
          </div>);
        childBlock =
          <div className='ContentMessages'>
            <h1 className='Content__h1'>My messages</h1>
            <div className='ContentMessages__wrapper'>
              {userInfo}
            </div>
          </div>;
        break;
      case '3':
        userInfo =
          <div className='ContentMessages__item _margin-top-20'>
            <span className='Content__label'>User login:</span>
            <h3 className='ContentMessages__h3 _margin-top-10'>{data.user__login}</h3>

            <span className='Content__label _margin-top-20'>Name:</span>
            <p className='Content__text _margin-top-10'>{data.user__firstName + ' ' + data.user__firstName}</p>
          </div>;

        childBlock =
          <div className='ContentUserInfo'>
            <h1 className='Content__h1'>About me</h1>
            <div className='ContentMessages__wrapper'>
              {userInfo}
            </div>
          </div>;
        break;
    }

    return (
      <div className={this.props.className + ' Content'}>
        {childBlock}
      </div>
    )
  }
}

