import React from 'react';

export default function Field(props) {
  let className = props.className ? props.className : '',
    type = props.type ? props.type : 'text';
  return (
    <div className={className + ' Field'}>
      <label htmlFor={props.id} className='Field__label'>{props.text}</label>
      {type === 'textarea' ?
        <textarea id={props.id} className='Field__textarea'/>
      :
        <input id={props.id} type={type} className='Field__input' autoComplete='off'/>
      }
    </div>
  )
}