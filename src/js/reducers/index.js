import {combineReducers} from 'redux';

import enterReducer from './enterReducer';
import contentReducer from './contentReducer';

export default combineReducers({
  registration: enterReducer,
  afterEnter: contentReducer
});