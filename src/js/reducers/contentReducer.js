import ACTIONTYPES from './actionTypes';
import data from '../data/data';

export default function contentReducer(state, action) {
  if (!state) {
    state = data;
  }

  const curState = state.users;

  switch (action.type) {
    case ACTIONTYPES.USER_MAKEMESSAGE:
      break;
    case ACTIONTYPES.USER_SENDMESSAGE:
      break;
    case ACTIONTYPES.USER_ALLMESSAGE:
      // let login = action.payload,
      //   usersLength = state.length, i, needUserMessages;
      //
      // for (i = 0; i < usersLength; i += 1) {
      //   console.log(curState[i].user__login);
      //   if (curState[i].user__login === login) {
      //     needUserMessages = curState[i].user__messages;
      //   }
      // }
      // return Object.assign({}, {
      //   user: needUserMessages
      // });
      return state;
    default:
      return state;
  }
}