import ACTIONTYPES from './actionTypes';
import data from '../data/data';

export default function enterReducer(state, action) {
  if (!state) {
    state = data;
  }
  switch (action.type) {
    case ACTIONTYPES.USER_REGISTRATION:
      break;
    case ACTIONTYPES.USER_ENTER:
      break;
    default:
      return state;
  }
}