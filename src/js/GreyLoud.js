import React from 'react';
import {connect} from 'react-redux';

import EnterBlock from './components/EnterBlock';
import AfterEnterBlock from './components/AfterEnterBlock';
import Exit from './components/Exit';

class GreyLoud extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      afterEnter: true,
      login: 'Empty Name'
    };

    this.exitOnClickHandler = this.exitOnClickHandler.bind(this);
  }

  exitOnClickHandler() {
    this.setState({
      afterEnter: false
    })
  }

  render() {

    return(
      <div className='main__Wrapper'>
        {
          this.state.afterEnter ?
            <AfterEnterBlock/>
            :
            <EnterBlock/>
        }
        {
          this.state.afterEnter ?
            <Exit login={this.state.login} exitClickHandler={this.exitOnClickHandler}/>
            :
            ''
        }

      </div>
    )
  }
}

export default connect(
  state => ({}),
  dispatch => ({})
)(GreyLoud);