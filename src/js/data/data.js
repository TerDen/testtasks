const data = {
  users: [
    {
      user__login: 'Petya',
      user__firstName: 'Petya',
      user__secondName: 'Ivanov',
      user__password: '123456789',
      user__messages: [
        {
          message__data: '11.05.2017',
          message__theme: 'sport',
          message__text: 'Hello, my name is Petya'
        },
        {
          message__data: '01.10.2016',
          message__theme: 'love',
          message__text: 'Hello, my name is Petya. I love you!'
        },
        {
          message__data: '15.01.2016',
          message__theme: 'cinema',
          message__text: "Hello, my name is Petya. Wouldn't you mind go to the cinema with me?"
        }
      ]
    },
    {
      user__login: 'Katya',
      user__firstName: 'Katya',
      user__secondName: 'Petrova',
      user__password: '987654321',
      user__messages: [
        {
          message__data: '23.08.2017',
          message__theme: 'sport',
          message__text: 'Hello, my name is Katya'
        },
        {
          message__data: '20.06.2017',
          message__theme: 'love',
          message__text: 'Hello, my name is Katya. I love you!'
        },
        {
          message__data: '15.01.2016',
          message__theme: 'money',
          message__text: "Do you know the rates of usd today?"
        }
      ]
    }
  ]
};

export default data;