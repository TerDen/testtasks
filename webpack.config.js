const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractPlugin = new ExtractTextPlugin({
  filename: './assets/css/app.css'
});

// Constant with our paths
const paths = {
  BUILD: path.resolve(__dirname, 'build'),
  SRC: path.resolve(__dirname, 'src'),
  JS: path.resolve(__dirname, 'src/js'),
};

let config = {
  context: paths.SRC,
  entry: path.join(paths.JS, 'main.js') ,
  devtool: 'inline-source-map',

  output: {
    path: paths.BUILD,
    filename: './js/[name].js'
  },

  devServer: {
    contentBase: paths.SRC,
    compress: true,
    port: 8080
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        include: /src/,
        exclude: /node_modules/,
        loader: 'babel-loader',

        query: {
          presets: ['es2015', 'react', 'stage-3']
        }
      },
      {
        test: /\.html$/,
        loader: ["html-loader"]
      },
      {
        test: /\.scss$/,
        include: [path.resolve(__dirname, 'src', 'assets', 'scss')],
        use: extractPlugin.extract({
          use: ["css-loader", "autoprefixer-loader", "sass-loader"],
          fallback: ['style-loader']
        })
      },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './assets/media/',
            }
          }
        ]
      }
    ]
  },
  plugins: [
    extractPlugin,
    new HtmlWebpackPlugin({
      template: 'index.html',
    }),
    new CleanWebpackPlugin(['build']),
    new CopyWebpackPlugin([{ from: './assets/libs', to: './assets/libs/' }]),
    new CopyWebpackPlugin([{ from: './assets/media', to: './assets/media/' }])
  ]
};

module.exports = config;